package vaccines;
import java.util.ArrayList;
import simulator.Persona;


interface Interfaz {
	public void Dosis(ArrayList<Persona> people);
	public int getDosisStep();
	public ArrayList<Persona> getPeople();
	public void setPeople(ArrayList<Persona> people);
	public void setNombre(String nombre);
	public String getNombre();
	public void setPoblacion(int poblacion);
	public int getVacunas();
	public void Vacunar(ArrayList<Persona> people);
}
