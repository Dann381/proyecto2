package vaccines;
import java.util.ArrayList;
import simulator.Persona;

//		--- Vacunas con dos dosis (en este caso, vax2 y vax3)
public class VaxTwoSteps extends Vaccine implements Interfaz{
	private int DosisStep;
	private ArrayList<Persona> people;
	public VaxTwoSteps(String nombre, Double porcentaje, int poblacion ,int DosisStep) {
		super(nombre, porcentaje, poblacion);
		this.DosisStep = DosisStep;
	}
	
	//		--- Aplicación de la segunda dosis
	public void Dosis(ArrayList<Persona> people) {
		this.setPeople(people);
		int Size = this.getPeople().size();
		for (int i=0; i < Size; i++) {
			if (this.getPeople().get(i).getVaxType() == "Vax2") {
				if (this.getPeople().get(i).getVaxStep() == this.DosisStep) {
					this.getPeople().get(i).setVaxType("Vax2(2)");
				}
			}
			else if (this.getPeople().get(i).getVaxType() == "Vax3") {
				if (this.getPeople().get(i).getVaxStep() == this.DosisStep) {
					this.getPeople().get(i).setVaxType("Vax3(2)");
				}
			}
		}
	}
			
	public int getDosisStep() {
		return this.DosisStep;
	}

	public ArrayList<Persona> getPeople() {
		return people;
	}

	public void setPeople(ArrayList<Persona> people) {
		this.people = people;
	}
}
