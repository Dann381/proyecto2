package vaccines;
import java.util.ArrayList;
import simulator.Persona;

public class Vaccine {
	
	private String nombre;
	private ArrayList<Persona> people;
	private Double porcentaje;
	private int poblacion;
	
	//Ingreso de nombre de la vacuna, porcentaje de vacunas que habrá, y la cantidad de personas de la población
	public Vaccine (String nombre, Double porcentaje, int poblacion) {
		
		this.nombre = nombre;
		this.porcentaje = porcentaje;
		this.poblacion = poblacion;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public void setPoblacion(int poblacion) {
		this.poblacion = poblacion;
	}
	public ArrayList<Persona> getPeople (){
		return this.people;
	}
	
	public int getVacunas() {
		int vacunas = (int) (this.poblacion * this.porcentaje);
		return vacunas;
	}
	
	//		--- Vacunación (primera dosis)
	public void Vacunar(ArrayList<Persona> people) {
		this.people = people;
		int Size = this.getPeople().size();
		int cantidad = (int)(Size * this.porcentaje);
		for (int i=0; i < cantidad; i++) {
			Boolean done = false;
			while (done == false) {
				int subject = (int)(Math.random()*Size);
				if (this.getPeople().get(subject).getImmunityStatus() == false) {
					this.getPeople().get(subject).setVacunado(true);
					this.getPeople().get(subject).setVaxType(this.getNombre());
					done = true;
				}
			}
		}
	}
}
