package simulator;

public class Enfermedad implements Interfaz {
	
	private double infectionChance;
	private double severityChance;
	private double deathChance;
	private int infectionSteps;
	private Boolean infectionResult;

	
	//		--- Builder
	Enfermedad (double infectionChance, int infectionSteps, double deathChance, double severityChance) {
		this.infectionChance = infectionChance;
		this.infectionSteps = infectionSteps;
		this.severityChance = severityChance;
		this.deathChance = deathChance;		
	}

	//		--- Obtener probabilidad de muerte y de gravedad
	public double getDeathChance () {
		return this.deathChance;
	}
	
	public double getSeverityChance () {
		return this.severityChance;
	}

	//		--- Dar la cantidad promedio de días de recuperación
	public int getInfectionSteps () {
		return this.infectionSteps;
	}

	//		--- Infectar a Alguien
	public Boolean infect (Persona subject) {
		if (Math.random()*1 <= this.infectionChance) {
			infectionResult = true;
		} 
		else {
			infectionResult = false;
		}
		return infectionResult;
	}
}
