package simulator;

public class Persona {
	
	private int id = 0;
	private String status = "Sano";
	private Boolean hasImmunity = false;
	private int daysInfected = 0;
	private Enfermedad disease;
	private int age = 1;
	private Afeccion afeccion;
	private String condicion = "Saludable";
	private Boolean Vacunado = false;
	private String vaxtype = "None";
	private int vaxstep = 0;

	//		--- Builder
	Persona (Enfermedad disease) {
		this.disease = disease;

	}	
	//		--- Definir la id única de la persona
	public void setId (int id) {
		this.id = id;
	}
	
	public int getId () {
		return this.id;
	}
	
	//		--- Definir la edad de la persona
	public void setAge (int age) {
		this.age = age;
	}
	
	public int getAge () {
		return this.age;
	}
	
	//		--- Definir la enfermedad base
	public void setAfeccion (Afeccion afeccion) {
		this.afeccion = afeccion;
	}
	
	public Afeccion getAfeccion () {
		return this.afeccion;
	}
	
	//		--- Definir la afección
	public void setCondicion (String condicion) {
		this.condicion = condicion;
	}
	
	public String getCondicion () {
		return this.condicion;
	}
	
	//		--- Definir el estado de la persona;
	public void setStatus (String status) {
		this.status = status;
	}
	
	public String getStatus () {
		return this.status;
	}
	
	//		--- Definir el estado de inmunidad de la persona
	public void setImmunity (Boolean hasImmunity) {
		this.hasImmunity = hasImmunity;
	}
	
	public Boolean getImmunityStatus () {
		return this.hasImmunity;
	}
	
	//		--- Días que ha estado infectado
	public int getDaysInfected () {
		return this.daysInfected;
	}
	
	//		--- Actualización de estado de vacunación de la persona
	public void setVacunado(Boolean vacunado) {
		this.Vacunado = vacunado;
	}
	
	public Boolean getVacunado() {
		return this.Vacunado;
	}
	
	public void setVaxType(String VaxType) {
		this.vaxtype = VaxType;
	}
	
	public String getVaxType() {
		return this.vaxtype;
	}
	
	public void addVaxStep() {
		this.vaxstep += 1;
	}
	
	public int getVaxStep() {
		return this.vaxstep;
	}	
	
	//		--- Checkeo para actualizar los datos
	public void check() {
		
		double SeverityChance = this.getAfeccion().getSeverityChance();
		double DeathChance = this.getAfeccion().getDeathChance();
		
		if (this.getVaxType() == "Vax1") {
			SeverityChance += -0.25;
			DeathChance += -0.25;
		}
		else if (this.getVaxType() == "Vax2(2)") {
			SeverityChance = 0.0;
			DeathChance = 0.0;
		}
		else if (this.getVaxType() == "Vax3(2)") {
			this.status = "Sano";
			this.hasImmunity = true;
		}
		//  Si está infectado y aún no pasan los días de la enfermedad
		if (this.status == "Infectado" && this.daysInfected < disease.getInfectionSteps()) {
				
			if (this.condicion != "Saludable") {
				SeverityChance += 0.1;
				DeathChance += 0.1;
			}
			
			if ((Math.random()*1) < SeverityChance) {
				this.setStatus("Grave");
				this.setImmunity(true);
			}
			this.daysInfected += 1;
		}
		
		// Si está grave y aún no pasan los días de la enfermedad
		else if (this.status == "Grave" && this.daysInfected < disease.getInfectionSteps()) {
			double ReturnInfect = 0.2;
			
			if ((Math.random()*1) < ReturnInfect) {
				this.setStatus("Infectado");
			}
			
			else if ((Math.random()*1) < DeathChance) {
				this.setStatus("Muerto");
				this.setImmunity(true);
			}
			this.daysInfected += 1;
		}
		// Si ya pasaron los días de la enfermedad	
		else if (this.daysInfected >= this.disease.getInfectionSteps()) {		
				this.setStatus("Recuperado");
				this.setImmunity(true);
		}
	}
}
