package simulator;

public class Afeccion {
	
	private double severityChance;
	private double deathChance;
	
	Afeccion(double severityChance, double deathChance) {
		this.severityChance = severityChance;
		this.deathChance = deathChance;
	}
	
	public double getSeverityChance() {
		return this.severityChance;
	}
	
	public double getDeathChance() {
		return this.deathChance;
	}
}
