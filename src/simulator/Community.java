package simulator;
import java.util.ArrayList;
import vaccines.Vaccine;
import vaccines.VaxTwoSteps;

public class Community {
	
	private String communityName;
	private int population;
	private int infected;
	private int totalInfected = 0;
	private int avgContact;
	private int normales;
	private int activos;
	private int graves;
	private int muertos;
	private int recuperados;
	private int sanos;
	private int tienen_afeccion;
	private int tienen_condicion;
	private double contactChance;
	private Enfermedad disease;
	private ArrayList<Persona> people = new ArrayList<Persona>();
	private ArrayList<Afeccion> afecciones = new ArrayList <Afeccion>();
	// Creación de las vacunas, Vax2 y Vax3 aplican su segunda dosis después de los días 3 y 6 de aplicar la primera dosis respectivamente
	private Vaccine vax1 = new Vaccine("Vax1", 0.25, people.size());
	private VaxTwoSteps vax2 = new VaxTwoSteps("Vax2", 0.16, people.size() ,3);
	private VaxTwoSteps vax3 = new VaxTwoSteps("Vax3", 0.08, people.size() ,6);
	
	
		//		--- Builder
		Community (String communityName,
				int population,
				int infected,
				int avgContact,
				double contactChance,
				Enfermedad disease,
				ArrayList<Afeccion> afecciones) {
		
			this.communityName = communityName;
			this.population = population;
			this.infected = infected;
			this.totalInfected += infected;
			this.avgContact = avgContact;
			this.contactChance = contactChance;
			this.disease = disease;
			this.afecciones = afecciones;
			this.vax1.setPoblacion(population);
			this.vax2.setPoblacion(population);
			this.vax3.setPoblacion(population);
		}
	
	
		//		--- Genera informacion de la comunidad
		public void startCommunity () {
			
			//		--- Crea cada persona con una edad al azar (entre 1 y 80 años), y todos con la afección en "Healthy"
			for (int i=1; i < this.population +1; i++) {
				people.add(new Persona(this.disease));
				people.get(i-1).setId(i);
				people.get(i-1).setAge((int)(Math.random()*85+1));
				people.get(i-1).setAfeccion(afecciones.get(5));
				}
			
			//		--- Generación de afecciones base para el 25% de la población
			int percBase = (int)(this.population * 0.25);
			for (int i=1; i < percBase + 1; i++) {
				Boolean baseDone = false;
				while (baseDone == false) {
					int random = (int) (Math.random()*5);
					int subject = (int)(Math.random()*this.population);
					if (people.get(subject).getAfeccion() == afecciones.get(5)) {
						people.get(subject).setAfeccion(afecciones.get(random));
						baseDone = true;
					}
				}
			}
			
			//		--- Generación de afecciones para el 65% de la población
			int percAfection = (int)(this.population * 0.65);
			for (int i=1; i < percAfection + 1; i++) {
				Boolean afectionDone = false;
				while (afectionDone == false) {
					int random = (int) (Math.random()*2+1);
					int subject = (int)(Math.random()*this.population);
					if (people.get(subject).getCondicion() == "Saludable") {
						if (random == 1) {
							people.get(subject).setCondicion("Obeso");
						}
						else if (random == 2) {
							people.get(subject).setCondicion("Desnutrido");
						}
					afectionDone = true;
					}
				}
			}		
			
			//		--- Generación de infectados iniciales
			for (int j=0; j < this.infected; j++) {
			
				Boolean infectionDone = false;
				while(infectionDone == false) {
				
				
					//	Posicion al azar del nuevo infectado
					int subject = (int)(Math.random()*this.population);
				
				
					//	Determinar si el posible nuevo infectado está infectado o no
					if (people.get(subject).getStatus() != "Infectado") {
					
						//	En caso de que esté "Sano" su estado pasará a "Infectado"
						people.get(subject).setStatus("Infectado");
						this.normales += 1;
						infectionDone = true;
					}
				}
			}
		}
	
	
		//		--- Ejecución tras dar un paso
		public void takeAStep (int step) {
			double stepContact = (Math.random()*1);
			if (stepContact <= contactChance) {
			
			
				//	Repetir el azar relacionado a la infección (cantidad de contactos) veces
				int contacts = this.normales * this.avgContact;
				for (int i = 0; i < contacts; i++) {
					Boolean stepInfectDone = false;
					while (stepInfectDone == false) {
			
					
						//  Posicion al azar del nuevo infectado
						int subject = (int)(Math.random()*this.population);
					
					
						//  Determinar si la persona ya está infectado o no
						if (people.get(subject).getStatus() == "Sano") {
						
						
							//  Determinar si la persona tiene inmunidad
							if (people.get(subject).getImmunityStatus() == false) {
							
							
								//  Realizar el azar de la infección 
								if (this.disease.infect(people.get(subject)) == true) {
								
								
									//  Actualizar los datos en caso de que salga positivo
									people.get(subject).setStatus("Infectado");
									this.totalInfected += 1;
									this.activos += 1;
									this.normales += 1;
								}
							}
						}
						stepInfectDone = true;
					}
				
				}
			
			}
			//		--- Vacunación
			for (int i = 0; i < people.size(); i++) {
				if (people.get(i).getVacunado() == true)  
					people.get(i).addVaxStep();
			}
			
			// Vacuna 1 se implementa en el día 10 del avance de la enfermedad
			if (step == 10) {
				vax1.Vacunar(people);
			}
			// Vacuna 2 y 3 (primera dosis) se implementan en el día 15
			if (step == 15) {
				vax2.Vacunar(people);
				vax3.Vacunar(people);
			}
			// Segunda dosis de cada vacuna se implementa después que hayan pasado los días determinados al crear cada vacuna
			vax2.Dosis(people);
			vax3.Dosis(people);
			
		}	//		--- Actualización de datos respecto al estado de las personas de la comunidad
		public void Actualizar() {
				for (int i = 0; i < people.size(); i++) {
					people.get(i).check();
				}
				this.tienen_afeccion = 0;
				this.tienen_condicion = 0;
				this.sanos = 0;
				this.normales = 0;
				this.graves = 0;
				this.activos = 0;
				this.recuperados = 0;
				this.muertos = 0;
				for (int i = 0; i < people.size(); i++) {
					if (people.get(i).getStatus() == "Sano") {
						this.sanos += 1;
					}
					else if (people.get(i).getStatus() == "Infectado") {
						this.normales += 1;
					}
					else if (people.get(i).getStatus() == "Grave") {
						this.graves += 1;
					}
					else if (people.get(i).getStatus() == "Muerto") {
						this.muertos += 1;
					}
					else if (people.get(i).getStatus() == "Recuperado") {
						this.recuperados += 1;
					}
					if (people.get(i).getAfeccion() != afecciones.get(5)) {
						this.tienen_afeccion += 1;
					}
					if (people.get(i).getCondicion() != "Saludable") {
						this.tienen_condicion += 1;
					}
				}
			this.activos = this.normales + this.graves;
		}
		
	
		//		--- Mostrar un informe general de la comunidad (Presenta este orden "Fragmentado" para que sea más facil continuar trabajando)
		public void report (int step) {
			int totalsanos = this.population - this.muertos;
			System.out.print("[Paso "+step+"] ");
			System.out.print("Total contagios: "+this.totalInfected);
			System.out.print(" || Activos: "+this.activos);
			/*System.out.print(" || Normales: "+this.normales);
			System.out.print(" || Graves: "+this.graves);
			System.out.print(" || Recuperados: "+this.recuperados);
			System.out.print(" || Muertos: "+this.muertos);
			System.out.print(" || Sin Contagiarse: "+this.sanos);
			System.out.print(" || Sobrevivientes: "+totalsanos);
			System.out.print(" || Con Afección: "+this.tienen_afeccion);
			System.out.print(" || Con Condición: "+this.tienen_condicion);*/
			System.out.println("");
		}
		
		//		--- Reporte realizado antes de comenzar con la propagación de la enfermedad
		public void InicialReport() {
			int edad = 0;
			for (int i = 0; i < people.size(); i++) {
				edad += people.get(i).getAge();
			}
			double promedio = edad/this.population;
			System.out.println("<<<<<<<<<< Datos de la comunidad >>>>>>>>>>\n");
			System.out.println("Total de población: " + this.population);
			System.out.print("Personas con enfermedad base: " + this.tienen_afeccion);
			System.out.print(" || Personas con alguna condición: " + this.tienen_condicion);
			System.out.println("\nEdad promedio de la comunidad: " + promedio);
			System.out.println("Vacunas Vax1: " + vax1.getVacunas());
			System.out.println("Vacunas Vax2: " + vax2.getVacunas());
			System.out.println("Vacunas Vax3: " + vax3.getVacunas());
		}
		
		//		--- Reporte realizado después de la propagación de la enfermedad
		public void FinalReport() {
			int edad = 0;
			int poblacion = 0;
			int tienen_afeccion = 0;
			int tienen_condicion = 0;
			int inoculados = 0;
			for (int i = 0; i < people.size(); i++) {
				if (people.get(i).getStatus() != "Muerto") {
						edad += people.get(i).getAge();
						poblacion += 1;
				
					if (people.get(i).getAfeccion() != this.afecciones.get(5)) {
						tienen_afeccion += 1;
					}
					if (people.get(i).getCondicion() != "Saludable") {
						tienen_condicion += 1;
					}
					if (people.get(i).getVacunado() == true) {
						inoculados += 1;
					}
				}
			}
			double promedio = edad/poblacion;
			System.out.println("\n<<<<<<<<<< Datos de la comunidad >>>>>>>>>>\n");
			System.out.println("Total de población viva: " + poblacion);
			System.out.print("Personas con enfermedad base: " + tienen_afeccion);
			System.out.print(" || Personas con alguna condición: " + tienen_condicion);
			System.out.println("\nInoculados (vivos): " + inoculados);
			System.out.println("Recuperados: " + this.recuperados);
			System.out.println("Fallecidos: " + this.muertos);
			System.out.println("Promedio edad comunidad viva: " + promedio);
		}
		
		//		--- Nombre de la comunidad
		public String getName () {
			return this.communityName;
		}
	
		//		--- Enfermedad de la comunidad
		public Enfermedad getDisease () {
			return this.disease;
		}
		
		//		--- Obtener casos activos (personas que tienen la enfermedad)
		public int getCasosActivos() {
			int casos = this.activos;
			return casos;
		}
}
