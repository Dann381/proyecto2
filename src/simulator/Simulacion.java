package simulator;

public class Simulacion {
	
	private Community workSpace;
	
	
	//		--- Builder
	Simulacion () {}
	
	
	//		--- Añadir comunidad
	public void addCommunity (Community workSpace) {
		this.workSpace = workSpace;
	}
	
	
	//		--- Empezar la simulación
	public void run () {
		// Actualización de datos de la comunidad y reporte
		workSpace.Actualizar();
		workSpace.InicialReport();
		System.out.println("\n~ Simulando enfermedad en "+workSpace.getName()+" ~ \n");
		
	//		--- Ejecución por pasos hasta que los infectados lleguen a 0
		int i = 1;
		workSpace.Actualizar();
		workSpace.report(i);
		i = 2;
			while (workSpace.getCasosActivos() > 0) {
				workSpace.takeAStep(i);
				workSpace.Actualizar();
				workSpace.report(i);
				i = i + 1;
			}
		System.out.println("\n~ Fin de la simulación ~");
		// Reporte final de la comunidad
		workSpace.FinalReport();
	}
}
