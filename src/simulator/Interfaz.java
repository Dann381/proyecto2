package simulator;

interface Interfaz {
	public double getDeathChance ();
	public double getSeverityChance ();
	public int getInfectionSteps ();
	public Boolean infect (Persona subject);
}
