package simulator;
import java.util.ArrayList;


public class Main {

	public static void main(String[] args) {
		
        //		--- Variables Establecidas
        // Enfermedad
        double probabilidadContagio = 0.12;
        int diasRecuperacion = 16;
        double mortalidad = 0.05;
        double gravedad = 0.1;
        
        // Comunidad
        String comunidadNombre = "Chile";
        int habitantes = 20000;
        int pacientes0 = 5;
        int promedioConexiones = 4;
        double probabilidadContacto = 0.75;
        
        // Afecciones
        double severitychance = 0.7;
        double deathchance = 0.6;
        
		//		--- Creación de la enfermedad
		Enfermedad disease = new Enfermedad(probabilidadContagio,	// Probabilidad de Contagio en un contacto cercano
											diasRecuperacion,		// Numero de pasos de la enfermedad antes de ser declarada (Muerto/ Curado)
											mortalidad,				// Probabilidad de morir a causa de la enfermedad
											gravedad);				// Probabilidad de caer en estado grave
		
		//		--- Creación de afecciones
		Afeccion asma = new Afeccion(severitychance, deathchance);
		Afeccion cerebrovascular = new Afeccion(severitychance, deathchance);
		Afeccion fibrosis = new Afeccion(severitychance, deathchance);
		Afeccion hipertension = new Afeccion(severitychance, deathchance);
		Afeccion presion = new Afeccion(severitychance, deathchance);
		Afeccion healthy = new Afeccion(gravedad, mortalidad);
		ArrayList<Afeccion> afecciones = new ArrayList <Afeccion>();
		afecciones.add(asma);
		afecciones.add(cerebrovascular);
		afecciones.add(fibrosis);
		afecciones.add(hipertension);
		afecciones.add(presion);
		afecciones.add(healthy);
		
		//		--- Creación de la comunidad
		Community workSpace = new Community(comunidadNombre,			// Nombre de la comunidad
											habitantes,					// Numero de habitantes de la comunidad
											pacientes0,					// Numero de infectados iniciales
											promedioConexiones,			// Promedio de contactos que tiene cada persona
											probabilidadContacto,		// Probabilidad de someterse a un contacto cercano
											disease,					// Enfermedad de la comunidad
											afecciones);				// Afecciones de la comunidad
		
		workSpace.startCommunity();		
		
		//		--- Creacion de la simulacion
		Simulacion simulation = new Simulacion();
		simulation.addCommunity(workSpace);
		
		//		--- Comienza la simulación
		simulation.run();
	}
}
