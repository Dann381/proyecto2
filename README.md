# Simulador de enfermedad en una comunidad

El presente proyecto de programación consiste en la simulación de un comunidad compuesta por personas con diferentes características y edades, para observar su comportamiento frente a la exposición de una enfermedad contagiosa, tal como el actual Covid-19.

### Comportamiento de la comunidad
Al estar en contacto con una persona infectada, el sujeto en cuestión puede pasar por diferentes etapas. Según un porcentaje de infectividad, el sujeto podría contagiarse o no. En caso de contagiarse, ésta sería la primera etapa, en la cual según un porcentaje de gravedad el sujeto podría caer en estado grave, teniendo la posibilidad de morir. Si el sujeto no cae en estado grave ni muere durante la enfermedad, éste se recuperará generando inmunidad.

Cada persona tiene diferentes características, tales como edad, enfermedad base y condición base.

* *El 25% de la población presenta una enfermedad base*: Asma, Fibrosis Quística, Enfermedad Cerebrovascular, Presión Arterial Alta o Hipertensión.
* *El 65% de la población presenta una condición base*: Obesidad o Desnutrición.

La generación de estas condiciones y enfermedades es totalmente al azar, por lo que una persona puede tener ambas características. El poseer alguna de estas características aumenta la probabilidad del sujeto de enfermar gravemente o morir.

### Creación de vacunas
Debido a que la simulación intenta imitar a la realidad, se han creado tres vacunas diferentes para que los sujetos de la comunidad que se hayan puesto la vacuna puedan hacerle frente a la enfermedad. Solo el 50% de la población aproximadamente tiene accesibilidad a una vacuna.
* Vax1: esta vacuna solo ofrece una reducción de los efectos de la enfermedad en un 25%, habiendo solo capacidad para vacunar a un 25% de la población.
* Vax2: esta vacuna ofrece una garantı́a de presentar solo sı́ntomas leves, esto quiere decir que el ciudadano no llega a estar grave o morir, pero a pesar de tener sı́ntomas leves aún sigue propagando la enfermedad. Solo existe capacidad para vacunar a un 16% de la población con esta vacuna, la cuál tiene dos dosis, aplicándose la segunda luego de tres días de la aplicación de la primera dosis.
* Vax3: esta vacuna ofrece la efectividad de ser inmune y no seguir propagando la enfermedad, pero solo el 8% de la población tiene la posibilidad de vacunarse. Esta vacuna tiene dos dosis, aplicándose la segunda luego de seis días de la aplicación de la primera dosis.
 
### Pre-requisitos

Para ejecutar correctamente el simulador, se debe tener instalado en el equipo el paquete de desarrollo de Java (JDK), ya que es una programación escrita en Java. En caso de no tenerlo, escribir:

```
apt install openjdk-11-jre
```

Si al ejecutar la línea de comando, no existe la versión 11, se puede descargar cualquier otra versión que sea superior a la versión 8 de JDK. Para revisar que versiones se encuentran disponibles en su equipo, escribir:

```
apt-cache search openjdk jre
```
### Ejecución
El archivo a ejecutar es "Main" que se encuentra en el paquete "simulator". Una vez ejecutado, se mostrará vía terminal los datos pre-simulación de la comunidad, tales como la cantidad de personas con una afección o condicion base, la cantidad de vacunas disponibles, el total de población, entre otras cosas.
Luego de esto, se iniciará la simulación, la cuál se realiza por pasos como si fueran días. Por cada día que transcurra, habrán más contagiados, los cuales podrán recuperarse, vacunarse, caer en estado grave e incluso morir, dependiendo del azar y las probabilidades escritas en el archivo "Main". La simulación se detiene hasta la cantidad de contagiados llega a 0.
Por último, se muestra un reporte de la comunidad post-enfermedad, evidenciando cómo fué afectada la población al estar en contacto con la enfermedad creada.
### Construido con

El presente proyecto de programación, se construyó en base al lenguaje de programación Java, en conjunto con el IDE "Eclipse IDE for Java Developers". El paquete de desarrollo utilizado fue la versión 11 de JDK.

* [Java] - Lenguaje de programación utilizado.
* [Eclipse](https://www.eclipse.org/downloads/) - IDE de Eclipse.

### Autores

* **Michelle Valdés** - Estudiante de segundo año de Ingeniería Civil en Bioinformática.
* **Daniel Tobar** - Estudiante de segundo año de Ingeniería Civil en Bioinformática.

